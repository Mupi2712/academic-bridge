import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'academic-bridge',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
