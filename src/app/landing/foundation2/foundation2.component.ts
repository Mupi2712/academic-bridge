import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-foundation2',
  templateUrl: './foundation2.component.html',
  styleUrls: ['./foundation2.component.scss'],
})
export class Foundation2Component implements OnInit {

  constructor(private route: Router) { }

  ngOnInit() {}

  toFoundation() {
    this.route.navigate(['/foundation']);
  }
}
