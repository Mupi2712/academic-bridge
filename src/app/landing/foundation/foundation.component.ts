import { Component, OnInit } from '@angular/core';
import { Foundation2Component} from '../foundation2/foundation2.component';
import {Router} from '@angular/router';

@Component({
  selector: 'app-foundation',
  templateUrl: './foundation.component.html',
  styleUrls: ['./foundation.component.scss'],
})
export class FoundationComponent implements OnInit {
  foundation2: Foundation2Component;
  constructor(private route: Router) { }

  ngOnInit() {}

  toFoundation2() {
    this.route.navigate(['/foundation2']);
  }

  toHome() {
    this.route.navigate(['/']);
  }
}
