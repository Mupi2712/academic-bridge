import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-federico',
  templateUrl: './federico.component.html',
  styleUrls: ['./federico.component.scss'],
})
export class FedericoComponent implements OnInit {
  s0 = 'Federico’s passion was to use technology to help all humans – TECH for' +
    ' GOOD.  He dedicated his life, inside and outside of work,' +
    ' to helping children working on things such as: \n';
  s1 = 'The Child Finder project' +
    ' – helped to find 130 human trafficking victims that were children' +
    ' within the first month it was released, Microsoft’s Artemis System ' +
    '– software designed to reduce online child sexual exploitation, Music as a Language' +
    ' – helping non-verbal children speak through music and Children’s' +
    ' Resource Exchange – accelerating access to coordinated healthcare resources for any child in need.  ';
  s2 = 'These are only a few of the projects he worked on over the many years he was with us. He used his knowledge,' +
    ' resources and life to leave the world in a better place than where he found it.\n';
  s3 = 'Federico touched so many lives with his kindness, compassion, knowledge, patience,' +
    ' desire to grow and grow others, and commitment to giving back; He left an imprint on the world that' +
    ' will never be forgotten.';
  content = this.s0+this.s1+this.s2+this.s3;

  constructor(private route: Router) {
  }

  ngOnInit() {
  }

  toHome() {
    this.route.navigate(['/']);
  }
}
