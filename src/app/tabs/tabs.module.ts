import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TabsPageRoutingModule } from './tabs-routing.module';

import { TabsPage } from './tabs.page';
import {HomePage} from './home/home.page';
import {FinancialPage} from './financial/financial.page';
import {ChatPage} from './chat/chat.page';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TabsPageRoutingModule
  ],
  declarations: [
    TabsPage,
    HomePage,
    FinancialPage,
    ChatPage
  ]
})
export class TabsPageModule {}
