import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FileNotFoundComponent} from './file-not-found/file-not-found.component';
import {LandingComponent} from './landing/landing.component';
import {AuthGuard} from './services/auth.guard';
import {FoundationComponent} from './landing/foundation/foundation.component';
import {FedericoComponent} from './landing/federico/federico.component';
import {RegisterComponent} from './login/register/register.component';
import {Foundation2Component} from './landing/foundation2/foundation2.component';

const routes: Routes = [
  {
    path: 'tabs',
    canActivate: [AuthGuard],
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule),
  },
  {
    path: 'providers',
    loadChildren: () => import('./providers/providers.module').then(m => m.ProvidersModule),
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule),
  },
  {
    path: 'foundation',
    component: FoundationComponent,
  },
  {
    path: 'foundation2',
    component: Foundation2Component,
  },
  {
    path: 'federico',
    component: FedericoComponent,
  },
  {
    path: 'register',
    component: RegisterComponent,
  },
  {
    path: '',
    component: LandingComponent,
  },
  {
    path: '**',
    component: FileNotFoundComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {}),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
