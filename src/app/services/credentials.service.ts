import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CredentialsService {
  private isLoggedIn = false;

  constructor() { }
  getLoggedIn(): boolean{
    return this.isLoggedIn;
  }
  login( username: string, password: string): boolean{
    if (username === 'Max' && password === '12345678'){
      this.isLoggedIn = true;
      return true;
    }
    return false;
  }
}
