import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ProvidersComponent} from './providers.component';
import {NewEntryComponent} from './new-entry/new-entry.component';

const routes: Routes = [
  {
    path: '',
    component: ProvidersComponent
  },
  {
    path: 'new-entry',
    component: NewEntryComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProvidersRoutingModule { }
