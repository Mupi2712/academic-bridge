import { Component, OnInit } from '@angular/core';
import {CredentialsService} from '../services/credentials.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginUserName: string;
  loginPw: string;

  constructor(private credentials: CredentialsService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {}

  login() {
    if (this.loginPw === '' || this.loginUserName === ''){
      alert('Enter your Credentials!');
      return;
    }
    if (this.credentials.login(this.loginUserName,this.loginPw)){
      const next = this.activatedRoute.snapshot.params.next || '/tabs';
      this.router.navigateByUrl(next);
    }else{
      this.loginUserName = '';
      this.loginPw = '';
      alert('Login failed!');
    }
  }

  loginGoogle() {
    const next = this.activatedRoute.snapshot.params.next || '/tabs';
    this.router.navigateByUrl(next);
  }

  loginOther() {
    const next = this.activatedRoute.snapshot.params.next || '/tabs';
    this.router.navigateByUrl(next);
  }

  loginMS() {
    const next = this.activatedRoute.snapshot.params.next || '/tabs';
    this.router.navigateByUrl(next);
  }
}
