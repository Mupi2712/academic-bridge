import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { IonicModule } from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  registerForm = new FormGroup(
    {
      lastname : new FormControl(null),
      firstname : new FormControl(null),
      email : new FormControl(null, [Validators.email]),
      password : new FormControl(null),
      passwordVerify : new FormControl(null),
      studyArea : new FormControl(null),
      birthday : new FormControl(null),
    }
  );

  constructor() {

  }

  ngOnInit() {}

  register() {
    if (this.registerForm.invalid) {
      console.log('form invalid');
      new Notification('Form is not valid!');
      return;
    }
    console.warn(this.registerForm.value);
  }
}
